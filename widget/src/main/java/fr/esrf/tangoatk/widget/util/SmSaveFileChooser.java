/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.esrf.tangoatk.widget.util;

import java.io.File;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

/**
 *
 * @author poncet
 */
public class SmSaveFileChooser extends JFileChooser
{
    private String                           rootDirectory = null;
    private SmSaveFileAccessoryPanel         saveAccessory = new SmSaveFileAccessoryPanel(400, 250);
    private JTable                           fileChoosertable = null;
    
    private java.awt.Dimension               increaseDimension = new java.awt.Dimension(220, 70);
    
    public SmSaveFileChooser ()
    {
        super();
        this.setAccessory(saveAccessory);        
        
        // Display the file list in details file list mode and sort the list according to
        // the last modified date.
        ActionMap am=this.getActionMap();
        Action details = am.get("viewTypeDetails");
        details.actionPerformed(null);
        
        fileChoosertable = SwingUtils.getDescendantsOfType(JTable.class, this).get(0);
        sortByDate();        
        validate();
        increaseSize(); 
                     
//        System.out.println("coucou");
    }
    
    public SmSaveFileChooser (String dirPath)
    {
        super(dirPath);
        rootDirectory = dirPath;
        this.setAccessory(saveAccessory);
        
        // Display the file list in details file list mode and sort the list according to
        // the last modified date.
        ActionMap am=this.getActionMap();
        Action details = am.get("viewTypeDetails");
        details.actionPerformed(null);
        
        fileChoosertable = SwingUtils.getDescendantsOfType(JTable.class, this).get(0);
        sortByDate();        
        validate();
        increaseSize(); 
    }
    
    void sortByDate()
    {
        if (fileChoosertable == null) return;
        SwingUtilities.invokeLater( new Runnable()
                                        {
                                           @Override
                                           public void run()
                                            {
                                                fileChoosertable.getRowSorter().toggleSortOrder(2);
                                                fileChoosertable.getRowSorter().toggleSortOrder(2);
                                            }
                                        }
                                   );        
    }
    
    public void increaseDimension(int w, int h)
    {
        increaseDimension = new java.awt.Dimension(w, h);
        increaseSize();
    }
    
    private void increaseSize()
    {
        if (increaseDimension == null) return;
        java.awt.Dimension  psize = this.getPreferredSize();
        psize.width = psize.width + increaseDimension.width;
        psize.height = psize.height + increaseDimension.height;
        this.setPreferredSize(psize);
    }

    
    public String getRootDirectory()
    {
        return(rootDirectory);
    }
     
    public void setRootDirectory(String dirPath)
    {
        File dir = new File(dirPath);
        super.setCurrentDirectory(dir);
        rootDirectory = dirPath;
    }
    
    public String getAuthorText()
    {
        if (saveAccessory == null) return null;
        return saveAccessory.getAuthorText();
    }
    
    public void setAuthorText(String auth)
    {
        if (saveAccessory == null) return;
        saveAccessory.setAuthorText(auth);
    }
    
    
    public String getCommentsText()
    {
        if (saveAccessory == null) return null;
        return saveAccessory.getCommentsText();
    }
    
    public void setCommentsText(String com)
    {
        if (saveAccessory == null) return;
        saveAccessory.setCommentsText(com);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                JFrame jf = new JFrame();
                jf.setVisible(true);
                
                SmSaveFileChooser saveFc = new SmSaveFileChooser("/users/poncet");
                
                int returnDial = saveFc.showDialog(jf.getRootPane(), "Save");
                if (returnDial == JFileChooser.CANCEL_OPTION)
                    return;
                
                java.io.File selectedFile = saveFc.getSelectedFile();
                if (selectedFile == null) return;

                if (!selectedFile.exists())
                {
                    JOptionPane.showMessageDialog(
                            jf.getRootPane(), "The selected file does not exist.\n\n",
                            "Save file aborted.\n",
                            javax.swing.JOptionPane.ERROR_MESSAGE);
                    return;
                }
                // do load action
                System.out.println("File to save : "+selectedFile.getName());
            }
        });
    }
    
}
